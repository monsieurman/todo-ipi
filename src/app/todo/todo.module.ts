import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoRoutingModule } from './todo-routing.module';
import { TodoListComponent } from './todo-list/todo-list.component';
import { HomeComponent } from './home/home.component';
import { TodoDetailsComponent } from './todo-details/todo-details.component';
import { TodoService } from './todo/todo.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [TodoListComponent, HomeComponent, TodoDetailsComponent],
  imports: [
    CommonModule,
    TodoRoutingModule,
    FormsModule
  ],
  providers: [
    TodoService
  ]
})
export class TodoModule { }
