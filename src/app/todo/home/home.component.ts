import { Component, OnInit } from '@angular/core';
import { TodoService, TodoList } from '../todo/todo.service';
import { Observable } from 'rxjs';
import { CreateTodoEvent, DeleteTodoEvent } from '../todo-list/todo-list.component';

@Component({
  selector: 'app-home',
  template: `
    <app-todo-list
      *ngFor="let list of lists | async"
      [list]="list"
      (createTodo)="createTodo($event)"
      (deleteTodo)="deleteTodo($event)"
      (deleteList)="deleteList($event)"
    ></app-todo-list>
    <input #input type="text" placeholder="New list" [(ngModel)]="newListName" (keyup)="createList($event)">
  `,
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  lists: Observable<TodoList[]> = this.todoService.todos$;
  newListName: string;

  constructor(
    private todoService: TodoService
  ) {
    this.todoService.fetchLists().subscribe(console.log);
  }

  createTodo(event: CreateTodoEvent) {
    this.todoService.addTodo(event);
  }

  deleteTodo(event: DeleteTodoEvent) {
    this.todoService.deleteTodo(event);
  }

  deleteList(id: number) {
    this.todoService.deleteList(id);
  }

  createList(event: KeyboardEvent) {
    if (event.code === 'Enter') {
      this.todoService.createList(this.newListName);
      this.newListName = '';
    }
  }
}
