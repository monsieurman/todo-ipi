import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TodoList, Todo } from '../todo/todo.service';

export interface DeleteTodoEvent {
  listID: number;
  todoID: number;
}

export interface CreateTodoEvent {
  listID: number;
  todoContent: string;
}

@Component({
  selector: 'app-todo-list',
  template: `
    <h2> {{ list.name }} </h2>
    <button aria-label="delete list" (click)="emmitDeleteList()"><i class="fas fa-trash-alt"></i></button>
    <app-todo-details
      *ngFor="let todo of list.todos"
      [todo]="todo"
      (delete)="emmitDeleteTodo($event)">
    </app-todo-details>
    <app-todo-details [todo]="newTodo" (valueChange)="emmitAddTodo()"></app-todo-details>
  `,
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent {
  @Input()
  list: TodoList;

  @Output()
  deleteTodo = new EventEmitter<DeleteTodoEvent>();

  @Output()
  deleteList = new EventEmitter<number>();

  @Output()
  createTodo = new EventEmitter<CreateTodoEvent>();

  newTodo: Todo =  {id: 9999999, content: ''};

  emmitDeleteTodo(todoID: number) {
    this.deleteTodo.emit({
      listID: this.list.id,
      todoID
    });
  }

  emmitDeleteList() {
    this.deleteList.emit(this.list.id);
  }

  emmitAddTodo() {
    this.createTodo.emit({
      listID: this.list.id,
      todoContent: this.newTodo.content
    });
    this.newTodo.content = '';
  }
}
