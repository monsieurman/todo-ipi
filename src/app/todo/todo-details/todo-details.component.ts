import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Todo } from '../todo/todo.service';

@Component({
  selector: 'app-todo-details',
  template: `
    <input #input placeholder="New todo" type="text" [(ngModel)]="todo.content" (keyup)="emitChange($event, input.value)">
    <button (click)="outputDelete()"><i class="fas fa-trash-alt"></i></button>
  `,
  styles: [`
    :host {
      display: flex;
      margin-bottom: 24px;
    }
    input {
      width: 100%;
      border: none;
      border-bottom: solid 1px lightgray;
    }
    input:focus {
      border-bottom: solid 1px blue;
    }
    button {
      border: none;
      cursor: pointer;
      background-color: transparent;
      font-size: 1rem;
    }
    button:hover {
      color: blue;
    }
  `]
})
export class TodoDetailsComponent {
  @Input()
  todo: Todo;

  @Output()
  delete = new EventEmitter<number>();

  @Output()
  valueChange = new EventEmitter<string>();

  outputDelete() {
    this.delete.emit(this.todo.id);
  }

  emitChange(event: KeyboardEvent, newValue: string) {
    if (event.code === 'Enter') {
      this.valueChange.emit(newValue);
    }
  }
}
