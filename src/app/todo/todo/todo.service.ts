import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/core/auth/auth.service';

import { map, tap } from 'rxjs/operators';
import { CreateTodoEvent, DeleteTodoEvent } from '../todo-list/todo-list.component';

interface TodoAPIResponse {
  password: string;
  username: string;
  todoListes: TodoAPIList[];
}

interface TodoAPIList {
  name: string;
  elements: string[];
}

export interface TodoList {
  id: number;
  name: string;
  todos: Todo[];
}

export interface Todo {
  id: number;
  content: string;
}

// Sequence for using as simple IDs for items
let ID_SEQ = 0;

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private apiURL = environment.apiURL;
  // tslint:disable-next-line: variable-name
  private _todos$: BehaviorSubject<TodoList[]> = new BehaviorSubject([]);

  get todos$(): Observable<TodoList[]> {
    return this._todos$.asObservable();
  }

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  fetchLists(): Observable<TodoList[]> {
    console.log('[Todo service] Fetch lists');
    return this.http.get<TodoAPIResponse>(this.apiURL + '/listes', {
      ...this.headers()
    }).pipe(
      map<TodoAPIResponse, TodoList[]>(t => this.toInternalFromAPI(t)),
      tap(listes => this._todos$.next(listes))
    );
  }

  createList(name: string) {
    console.log('[Todo service] Add list', name);
    const nextValue: TodoList[] = [
      ...this._todos$.value,
      {id: this.nextId(), name, todos: [] }
    ];
    this.updateLists(nextValue);
  }

  addTodo(event: CreateTodoEvent) {
    console.log('[Todo service] Add todo', event);
    const nextValue = this._todos$.value.map(list => (list.id !== event.listID) ? list : ({
      ...list,
      todos: [
        ...list.todos,
        { id: this.nextId(), content: event.todoContent }
      ]
    }));
    this.updateLists(nextValue);
  }

  deleteTodo(event: DeleteTodoEvent) {
    console.log('[Todo service] Delete todo', event);
    const nextValue = this._todos$.value.map(list => (list.id !== event.listID) ? list : ({
      ...list,
      todos: list.todos.filter(t => t.id !== event.todoID)
    }));
    this.updateLists(nextValue);
  }

  deleteList(listID: number) {
    console.log('[Todo Service] Delete list', listID);
    const nextValue = this._todos$.value.filter(list => list.id !== listID);
    this.updateLists(nextValue);
  }

  updateLists(newValue: TodoList[]): Observable<TodoList[]> {
    console.log('[Todo Service] Update lists', newValue);
    this._todos$.next(newValue);
    return this._todos$.asObservable();
    // TODO: Add debounce
    // TODO: Replace by API call
    return this.http.get<TodoAPIResponse>(this.apiURL + '/listes', {
      ...this.headers()
    }).pipe(
      map<TodoAPIResponse, TodoList[]>(t => this.toInternalFromAPI(t)),
      tap(listes => this._todos$.next(listes))
    );
  }

  private toInternalFromAPI(t: TodoAPIResponse): TodoList[] {
    return t.todoListes.map<TodoList>(list => ({
      name: list.name,
      id: this.nextId(),
      todos: list.elements.map(content => ({
        id: this.nextId(),
        content
      }))
    }));
  }

  private headers() {
    return {
      headers: {
        login: this.authService.credentials.login,
        password: this.authService.credentials.password
      }
    };
  }

  private nextId(): number {
    return ID_SEQ++;
  }
}
