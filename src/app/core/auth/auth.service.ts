import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

export interface Credentials {
  login: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  set credentials(creds: Credentials) {
    localStorage.setItem('credentials', JSON.stringify(creds));
  }

  get credentials(): Credentials {
    const creds = localStorage.getItem('credentials');
    return creds
      ? JSON.parse(creds)
      : null;
  }

  private apiURL = environment.apiURL;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  async login(credentials: Credentials) {
    const res = await this.http.get(`${this.apiURL}/create/${credentials.login}/${credentials.password}`).toPromise();
    localStorage.setItem('credentials', JSON.stringify(credentials));
  }


}
