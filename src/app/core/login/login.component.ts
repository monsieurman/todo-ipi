import { Component, OnInit } from '@angular/core';
import { AuthService, Credentials } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  credentials: Credentials = {
    login: '',
    password: ''
  };
  pwdConf = '';

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    const credentials = localStorage.getItem('credentials');
    if (credentials) {
      this.router.navigateByUrl('/todo');
    }
  }

  login() {
    this.auth.login(this.credentials);
    this.router.navigateByUrl('/todo');
  }
}
